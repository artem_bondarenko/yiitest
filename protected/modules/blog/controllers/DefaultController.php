<?php

class DefaultController extends RController {
  public $layout='//layouts/column2';

  public function actionIndex() {
    $criteria = new CDbCriteria(array(
      'condition' => 'parentId IS NULL',
      'order' => 'id DESC'
    ));

    $dataProvider = new CActiveDataProvider('Blog', array(
      'pagination' => array(
        'pageSize' => 3,
      ),
      'criteria' => $criteria,
    ));

    $this->render('index',array(
      'dataProvider' => $dataProvider,
    ));
  }

  public function actionMark($id, $mark) {
    $msg = $this->loadModel($id);
    $msg->marked = $mark ? 1 : 0;
    $msg->save();

    $this->redirect(
      array('index')
    );
  }

	public function filters() {
		return array(
      'rights',
		);
	}

  public function actionDelete($id) {
    $this->loadModel($id)->delete();

    if ( !isset($_GET['ajax']) ) {
      $this->redirect(
        isset($_POST['returnUrl'])
          ? $_POST['returnUrl']
          : array('index')
      );
    }
  }

  public function actionCreate( $parentId = null ) {
    $model = new Blog;

    $parent = $parentId
      ? $this->loadModel($parentId)
      : null;

    if( isset($_POST['Blog']) ) {
      $model->userId = Yii::app()->user->id;
      $model->parentId = $parentId;
      $model->text = $_POST['Blog']['text'];
      $model->save();

      $this->redirect(
        array('index')
      );
    }

    $this->render('create', array(
      'model' => $model,
      'parent' => $parent
    ));
  }

	public function actionAdmin() {
		$model = new Blog('search');
		$model->unsetAttributes();

		if ( isset($_GET['Blog']) ) {
			$model->attributes = $_GET['Blog'];
    }

		$this->render('admin',array(
			'model' => $model,
		));
	}

	public function loadModel($id) {
		$model = Blog::model()->findByPk($id);

		if ( null === $model ) {
			throw new CHttpException( 404,'The requested page does not exist.' );
    }

		return $model;
	}


}
