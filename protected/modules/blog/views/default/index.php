<?php
/* @var $this DefaultController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Блоги',
);

$this->menu = array();

if ( !Yii::app()->user->isGuest ) {
  $this->menu[] = array('label' => 'Написать сообщение', 'url' => array('create'));
}
?>

<h1>Блоги</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider' => $dataProvider,
	'itemView' => '_view',
)); ?>


<script>
  $('.blog-item-remove').click(function() {
    return confirm('Уверены, что хотите удалить сообщение?');
  });
</script>