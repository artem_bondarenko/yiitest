<?php
/* @var $this DefaultController */
/* @var $model Blog */

$this->breadcrumbs=array(
	'Блоги' => array('index'),
	$parent ? 'Ответить' : 'Создать',
);

$this->menu=array(
	array('label'=>'Все сообщения', 'url'=>array('index'))
);
?>

<h1><?php echo $parent ? "Ответить на сообщение «{$parent->text}»" : 'Создать сообщение' ?></h1>

<?php $this->renderPartial(
  '_form',
  array(
    'model' => $model,
  )
); ?>