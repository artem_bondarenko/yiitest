<?php
/* @var $this DefaultController */
/* @var $data Blog */
?>

<div class="blog-items">
  <div class="blog-item <?php if ($data->marked) : ?>blog-item-marked<?php endif ?>">
    <div class="blog-item-message">
      <?php echo CHtml::encode($data->text); ?>
    </div>

    <div class="blog-item-userinfo">
      <?php echo CHtml::encode($data->user->username); ?>
    </div>

    <div class="blog-item-buttons">
      <?php if ( Yii::app()->user->checkAccess('Moderator') ): ?>
        <?php echo CHtml::link(
          $data->marked ? 'Снять пометку' : 'Пометить',
          array('mark', 'id' => $data->id, 'mark' => !$data->marked),
          array('class' => 'blog-item-mark')
        ); ?>
      <?php endif ?>

      <?php if ( !Yii::app()->user->isGuest ): ?>
        <?php echo CHtml::link(
          'Ответить',
          array('create', 'parentId' => $data->id),
          array('class' => 'blog-item-answer')
        ); ?>
      <?php endif ?>
    </div>

    <div class="clr"></div>

    <?php if ( Yii::app()->user->checkAccess('Admin') ): ?>
      <?php echo CHtml::link(
        'Удалить',
        array('delete', 'id' => $data->id),
        array('class' => 'blog-item-remove')
      ); ?>
    <?php endif ?>
  </div>

  <?php if ( $answers = $data->answers ): ?>
    <div class="blog-item-answers">
      <?php foreach ( $answers as $data ) {
        include '_view.php';
      } ?>
    </div>
  <?php endif ?>
</div>